#ifndef HEAD_H
#define HEAD_H
#include <QtTest>
#include "../functions.h"

class test : public QObject
{
    Q_OBJECT

public:
    test();

private slots:
    void test_equal_arrays();
    void test_Search_for_element();
    void test_Search_for_three_biggest();
    void test_Sum_of_main_diagonal();
    void test_Sum_of_side_diagonal();
    void test_Even_indexes();
    void test_Reflection();
    void test_Marks();
};

#endif // HEAD_H
