#include <head.h>

// add necessary includes here



test::test()
{

}
bool Equal_1arrays(QVector<int> ar_1, QVector<int> ar_2)
{
    return ar_1 == ar_2;
}

void test::test_equal_arrays()
{
    QVector<QVector<int>> example_1 = {
        {1, 1, 1},
        {1, 1, 1},
        {1, 1, 1},
    };

    QCOMPARE(1, Equal_arrays(example_1, example_1));

    QVector<QVector<int>> example_2 = {
        {1, 1, 1}
    };
    QCOMPARE(0, Equal_arrays(example_1, example_2));

    example_2 = {
            {1},
            {1},
            {1},
            {1},
        };
    QCOMPARE(0, Equal_arrays(example_1, example_2));

    QVector<QVector<int>> example_3 = {
        {1, 1, 1, 1},
        {1, 1, 1, 1},
        {1, 1, 1, 1},
        {1, 1, 1, 1},
    };
    QCOMPARE(0, Equal_arrays(example_1, example_3));

    QVector<QVector<int>> example_4 = {
        {2, 3, 4},
        {7, 6, 5},
        {8, 9, 1},
    };
    QCOMPARE(0, Equal_arrays(example_1, example_4));
}

void test::test_Search_for_element()
{
    QVector<QVector<int>> example_1 = {
        {1, 1, 1},
        {1, 1, 1},
        {1, 1, 1},
    };
    QVector<int> result = {0, 0};

    QCOMPARE(true, Equal_1arrays(Search_for_element(example_1, 1), result));

    result = {-1, -1};
    QCOMPARE(true, Equal_1arrays(Search_for_element(example_1, 2), result));

    example_1 = {
        {2, 3, 4, 10},
        {7, 6, 5, 11},
        {8, 9, 1, 12},
    };
    result = {1, 3};
    QCOMPARE(true, Equal_1arrays(Search_for_element(example_1, 11), result));

    example_1 = {
        {45, 68, 44, -68, 59},
    };
    result = {0, 1};
    QCOMPARE(true, Equal_1arrays(Search_for_element(example_1, 68), result));

    example_1 = {
            {0},
            {-2},
            {33},
            {-120},
        };
    result = {1, 0};
    QCOMPARE(true, Equal_1arrays(Search_for_element(example_1, -2), result));
}

void test::test_Search_for_three_biggest()
{
    QVector<QVector<int>> example_1 = {
        {1, 1, 1},
        {1, 1, 1},
        {1, 1, 1},
    };
    QVector<int> result = {1, 1, 1};

    QCOMPARE(true, Equal_1arrays(Search_for_three_biggest(example_1), result));

    QVector<QVector<int>> example_2 = {
        {2, 3, 4, 10, 0},
        {7, 6, 5, 11, -5},
        {8, 9, 1, 12, 12},
    };
    result = {12, 11, 10};
    QCOMPARE(true, Equal_1arrays(Search_for_three_biggest(example_2), result));

    QVector<QVector<int>> example_3 = {
        {45, 68, 44, -68, 59},
    };
    result = {68, 59, 45};
    QCOMPARE(true, Equal_1arrays(Search_for_three_biggest(example_3), result));

    QVector<QVector<int>> example_4 = {
            {0},
            {-2},
            {33},
            {-120},
        };
    result = {33, 0, -2};
    QCOMPARE(true, Equal_1arrays(Search_for_three_biggest(example_4), result));

    QVector<QVector<int>> example_5 = {
            {0, 0},
            {-2, 0},
        };
    result = {0, 0, -2};
    QCOMPARE(true, Equal_1arrays(Search_for_three_biggest(example_5), result));
}

void test::test_Sum_of_main_diagonal()
{
    QVector<QVector<int>> example_1 = {
        {1, 1, 1},
        {1, 1, 1},
        {1, 1, 1},
    };

    QCOMPARE(3, Sum_of_main_diagonal(example_1));

    QVector<QVector<int>> example_2 = {
        {2, 3, 4, 10, 0},
        {7, 6, 5, 11, -5},
        {8, 9, 1, 12, 12},
    };
    QCOMPARE(9, Sum_of_main_diagonal(example_2));

    QVector<QVector<int>> example_3 = {
        {45, 68, 44, -68, 59},
    };
    QCOMPARE(45, Sum_of_main_diagonal(example_3));

    QVector<QVector<int>> example_4 = {
            {0},
            {-2},
            {33},
            {-120},
        };
    QCOMPARE(0, Sum_of_main_diagonal(example_4));

    QVector<QVector<int>> example_5 = {
            {0, 0},
            {0, -2},
        };
    QCOMPARE(-2, Sum_of_main_diagonal(example_5));
}

void test::test_Sum_of_side_diagonal()
{
    QVector<QVector<int>> example_1 = {
        {1, 1, 1},
        {1, 1, 1},
        {1, 1, 1},
    };

    QCOMPARE(3, Sum_of_side_diagonal(example_1));

    QVector<QVector<int>> example_2 = {
        {2, 3, 4, 10, 0},
        {7, 6, 5, 11, -5},
        {8, 9, 1, 12, 12},
    };
    QCOMPARE(12, Sum_of_side_diagonal(example_2));

    QVector<QVector<int>> example_3 = {
        {45, 68, 44, -68, 59},
    };
    QCOMPARE(59, Sum_of_side_diagonal(example_3));

    QVector<QVector<int>> example_4 = {
            {0},
            {-2},
            {33},
            {-120},
        };
    QCOMPARE(0, Sum_of_side_diagonal(example_4));

    QVector<QVector<int>> example_5 = {
            {0, 0},
            {-2, 0},
        };
    QCOMPARE(-2, Sum_of_side_diagonal(example_5));
}

void test::test_Even_indexes()
{
    QVector<QVector<int>> example_1 = {
        {1, 1, 1},
        {1, 1, 1},
        {1, 1, 1},
    };

    QCOMPARE(5, Sum_even_indexes(example_1));

    QVector<QVector<int>> example_2 = {
        {2, 3, 4, 10, 0},
        {7, 6, 5, 11, -5},
        {8, 9, 1, 12, 12},
    };
    QCOMPARE(44, Sum_even_indexes(example_2));

    QVector<QVector<int>> example_3 = {
        {45, 68, 44, -68, 59},
    };
    QCOMPARE(148, Sum_even_indexes(example_3));

    QVector<QVector<int>> example_4 = {
            {0},
            {-2},
            {33},
            {-120},
        };
    QCOMPARE(33, Sum_even_indexes(example_4));

    QVector<QVector<int>> example_5 = {
            {0, 0},
            {-2, 0},
        };
    QCOMPARE(0, Sum_even_indexes(example_5));
}

void test::test_Reflection()
{
    QVector<QVector<int>> example_1 = {
        {1, 1, 1},
        {1, 1, 1},
        {1, 1, 1},
    };

    QVector<QVector<int>> reflected = {
        {1, 1, 1},
        {1, 1, 1},
        {1, 1, 1},
    };
    Reflection(example_1);
    QCOMPARE(1, Equal_arrays(example_1, reflected));

    QVector<QVector<int>> example_2 = {
        {2, 3, 4, 10, 0},
        {7, 6, 5, 11, -5},
        {8, 9, 1, 12, 12},
    };
    reflected = {
        {0, 10, 4, 3, 2},
        {-5, 11, 5, 6, 7},
        {12, 12, 1, 9, 8},
    };
    Reflection(example_2);
    QCOMPARE(1, Equal_arrays(example_2, reflected));

    QVector<QVector<int>> example_3 = {
        {45, 68, 44, -68, 59},
    };
    reflected = {
        {59, -68, 44, 68, 45},
    };
    Reflection(example_3);
    QCOMPARE(1, Equal_arrays(example_3, reflected));

    QVector<QVector<int>> example_4 = {
            {0},
            {-2},
            {33},
            {-120},
        };
    reflected = {
        {0},
        {-2},
        {33},
        {-120},
    };
    Reflection(example_4);
    QCOMPARE(1, Equal_arrays(example_4, reflected));

    QVector<QVector<int>> example_5 = {
            {3, 1, 0, 0},
            {9, 4, -2, 0},
        };
    reflected = {
        {0, 0, 1, 3},
        {0, -2, 4, 9},
    };
    Reflection(example_5);
    QCOMPARE(1, Equal_arrays(example_5, reflected));
}

void test::test_Marks()
{
    QCOMPARE(5, Average_mark(5, 5, 5, 5));
}
