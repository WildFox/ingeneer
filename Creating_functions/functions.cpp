#include "functions.h"

bool Equal_arrays(QVector<QVector<int>> &ar_1, QVector<QVector<int>> &ar_2)
{
    if(ar_1.size() != ar_2.size() || ar_1[0].size() != ar_2[0].size())
    {
        return 0;
    }
    for (unsigned int i = 0; i < ar_1.size(); i++)
    {
        if (ar_1[i] != ar_2[i])
        {
            return 0;
        }
    }
    return 1;
}

QVector<int> Search_for_element(QVector<QVector<int>> &ar, int element)
{
    QVector<int> indexes = {-1, -1};
    for (int i = 0; i < ar.size(); i++)
    {
        for (int j = 0; j < ar[0].size(); j++)
        if (ar[i][j] == element)
        {
            indexes[0] = i;
            indexes[1] = j;
            return indexes;
        }
    }
    return indexes;
}

QVector<int> Search_for_three_biggest(QVector<QVector<int>> &ar)
{
    QVector<int> mono_ar;

    for (int i = 0; i < ar.size(); i++)
    {
        for (int j = 0; j < ar[0].size(); j++)
        {
            mono_ar.push_back(ar[i][j]);
        }
    }

    for (int i = 0; i < mono_ar.size() - 1; i++)
    {
        for (int j = 0; j < mono_ar.size() - 1; j++)
        {
            if (mono_ar[j] < mono_ar[j + 1])
            {
                int tmp = mono_ar[j];
                mono_ar[j] = mono_ar[j + 1];
                mono_ar[j + 1] = tmp;
            }
        }
    }
}

int Sum_of_main_diagonal(QVector<QVector<int>> &ar)
{
    int sum = 0;

    for(int i = 0; i < ar.size() && i < ar[0].size(); i++)
    {
        sum += ar[i][i];
    }

    return sum;
}

int Sum_of_side_diagonal(QVector<QVector<int>> &ar)
{
    int i = 0;
    int j = ar[0].size() - 1;
    int sum = 0;
    while (i < ar.size() && j >= 0)
    {
        sum += ar[i][j];
        i++;
        j--;
    }
    return sum;
}

int Sum_even_indexes(QVector<QVector<int>> &ar)
{
    int sum = 0;

    for (int i  = 0; i < ar.size(); i++)
    {
        for (int j = 0; j < ar[0].size(); j++)
        {
            if ((i + j) % 2 == 0)
            {
                sum += ar[i][j];
            }
        }
    }
    return sum;
}

void Reflection(QVector<QVector<int>> &ar)
{
    for (int i  = 0; i < ar.size(); i++)
    {
        for (int j = 0; j < (ar[0].size() / 2); j++)
        {
            int tmp = ar[i][j];
            ar[i][j] = ar[i][ar[0].size() - 1 - j];
            ar[i][ar[0].size() - 1 - j] = tmp;
        }
    }
}

double Average_mark(int a, int b, int c, int d)
{
    return (a+b+c+d)/4;
}
