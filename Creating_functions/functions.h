#ifndef FUNCTIONS_H
#define FUNCTIONS_H
#include <QVector>
#include <string>
using namespace std;

bool Equal_arrays(QVector<QVector<int>> &ar_1, QVector<QVector<int>> &ar_2);
QVector<int> Search_for_element(QVector<QVector<int>> &ar, int element);
QVector<int> Search_for_three_biggest(QVector<QVector<int>> &ar);
int Sum_of_main_diagonal(QVector<QVector<int>> &ar);
int Sum_of_side_diagonal(QVector<QVector<int>> &ar);
int Sum_even_indexes(QVector<QVector<int>> &ar);
void Reflection(QVector<QVector<int>> &ar);
double Average_mark(int a, int b, int c, int d);
#endif // FUNCTIONS_H
