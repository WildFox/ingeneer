#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <string>
#include <QVector>

using namespace std;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}
QVector<QVector<int>> Main_matrix()
{
    string string_matrix = ui->first_array->text().toStdString();
    QVector<QVector<int>> matrix;
    int k = 0;

    for(unsigned int i = 0; i < sizeof(string_matrix); i++)
    {
        while(string_matrix[i] != ';')
        {
            while(string_matrix[i] != ',')
            {
                matrix[k].push_back(stoi(string_matrix[i]));
            }
        }
    }
}
bool Equal_arrays(QVector<QVector<int>> &ar_1, QVector<QVector<int>> &ar_2)
{
    if(ar_1.size() != ar_2.size() || ar_1[0].size() != ar_2[0].size())
    {
        return 0;
    }
    for (unsigned int i = 0; i < ar_1.size(); i++)
    {
        if (ar_1[i] != ar_2[i])
        {
            return 0;
        }
    }
    return 1;
}

QVector<int> Search_for_element(QVector<QVector<int>> &ar, int element)
{
    QVector<int> indexes = {-1, -1};
    for (int i = 0; i < ar.size(); i++)
    {
        for (int j = 0; j < ar[0].size(); j++)
        if (ar[i][j] == element)
        {
            indexes[0] = i;
            indexes[1] = j;
            return indexes;
        }
    }
    return indexes;
}

QVector<int> Search_for_three_biggest(QVector<QVector<int>> &ar)
{
    QVector<int> mono_ar;

    for (int i = 0; i < ar.size(); i++)
    {
        for (int j = 0; j < ar[0].size(); j++)
        {
            mono_ar.push_back(ar[i][j]);
        }
    }

    for (int i = 0; i < mono_ar.size() - 1; i++)
    {
        for (int j = 0; j < mono_ar.size() - 1; j++)
        {
            if (mono_ar[j] < mono_ar[j + 1])
            {
                int tmp = mono_ar[j];
                mono_ar[j] = mono_ar[j + 1];
                mono_ar[j + 1] = tmp;
            }
        }
    }

    QVector<int> biggest = {mono_ar[0], mono_ar[1], mono_ar[2]};

    int i = 1;
    while (biggest[1] == biggest[2] && i < mono_ar.size())
    {
        if (biggest [2] != mono_ar[i])
        {
            biggest[2] = mono_ar[i];
        }
        i++;
    }

    while (biggest[0] == biggest[1] && i < mono_ar.size())
    {
        if (biggest [1] != mono_ar[i] && biggest[2] != mono_ar[i])
        {
            biggest[1] = biggest[2];
            biggest[2] = mono_ar[i];
        }
        i++;
    }

    return biggest;
}

int Sum_of_main_diagonal(QVector<QVector<int>> &ar)
{
    int sum = 0;

    for(int i = 0; i < ar.size() && i < ar[0].size(); i++)
    {
        sum += ar[i][i];
    }

    return sum;
}

int Sum_of_side_diagonal(QVector<QVector<int>> &ar)
{
    int i = 0;
    int j = ar[0].size() - 1;
    int sum = 0;
    while (i < ar.size() && j >= 0)
    {
        sum += ar[i][j];
        i++;
        j--;
    }
    return sum;
}

int Sum_even_indexes(QVector<QVector<int>> &ar)
{
    int sum = 0;

    for (int i  = 0; i < ar.size(); i++)
    {
        for (int j = 0; j < ar[0].size(); j++)
        {
            if ((i + j) % 2 == 0)
            {
                sum += ar[i][j];
            }
        }
    }
    return sum;
}

void Reflection(QVector<QVector<int>> &ar)
{
    for (int i  = 0; i < ar.size(); i++)
    {
        for (int j = 0; j < (ar[0].size() / 2); j++)
        {
            int tmp = ar[i][j];
            ar[i][j] = ar[i][ar[0].size() - 1 - j];
            ar[i][ar[0].size() - 1 - j] = tmp;
        }
    }
}

void MainWindow::on_comparing_clicked()
{
    string string_matrix = ui->first_array->text().toStdString();
    QVector<QVector> matrix_1

    for(unsigned int i = 0; i < sizeof(string_matrix); i++)
    {

    }

    string_matrix = ui->second_array->text().toStdString();

}
